<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersImportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::get('/', [UsersImportController::class, 'show']);
Route::post('users/import', [UsersImportController::class, 'store']);

Route::get('users/download', [UsersImportController::class, 'download']);