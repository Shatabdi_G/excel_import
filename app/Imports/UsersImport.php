<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class UsersImport implements 
    ToModel,
   
    WithHeadingRow, 
    SkipsOnError, 
    WithValidation,
    SkipsOnFailure
{
    use Importable, SkipsErrors, SkipsFailures;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $user = User::create([
                'name' => $row['name'],
                'email' => $row['email'],
                'password' => Hash::make('password'),
            ]);
        
        $user->address()->create([
                'address' => $row['address'],
            ]);
        
        if($row['type'] == 'school')
        {
            $user->schools()->attach($row['id']);                     
        }
        elseif($row['type'] == 'job')
        {
            $user->jobs()->attach($row['id']);
        }
        return $user;
    }
    // public function collection(Collection $rows)
    // {
    //    foreach($rows as $row)
    //    {
    //        $user = User::create([
    //            'name' => $row['name'],
    //            'email' => $row['email'],
    //            'password' => Hash::make('password'),
    //        ]);

    //        $user->address()->create([
    //            'address' => $row['address'],
    //        ]);

    //        if($row['type'] == 'school')
    //        {
    //            $user->schools()->attach($row['id']);
             
    //        }
    //        elseif($row['type'] == 'job')
    //        {
    //            $user->jobs()->attach($row['id']);
    //        }
           
    //    }
    // }

    public function rules(): array
    {
        return[
            '*.email' => ['email', 'unique:users,email'],
        ];

    }
  
}
