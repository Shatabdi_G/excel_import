<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Validators\ValidationException;

class UsersImportController extends Controller
{
    public function show()
    {
        return view('users.import');
    }
    public function store(Request $request)
    {
        $file = $request->file('file');
        $fileName = $request->file->getClientOriginalName();
        $filePath =  $request->file->storeAs('import', $fileName);

        // Excel::import(new UsersImport, $file);

        $import = new UsersImport;
        $import->import($file);

        if ($import->failures()->isNotEmpty()) 
        {
            return back()->withFailures($import->failures());
        }
        // dd($import->failures());
        return back()->withStatus('Excel file successfully imported!');
    }

    public function download()
    {
        return response()->download(public_path('storage/data_format.xlsx'));
    }
}
